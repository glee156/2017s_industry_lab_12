package ictgradschool.industry.lab12.bounce;

import java.awt.*;
import java.util.ArrayList;

/**
 * Class to represent a simple rectangle.
 * 
 * @author Ian Warren
 */
public class DynamicRectangleShape extends DynamicShape {
	/**
	 * Default constructor that creates a ictgradschool.industry.lab12.bounce.RectangleShape instance whose instance
	 * variables are set to default values.
	 */
	public DynamicRectangleShape() {
		super();
	}

	public DynamicRectangleShape(int x, int y){
		super(x, y);
	}

	/**
	 * Creates a ictgradschool.industry.lab12.bounce.RectangleShape instance with specified values for instance
	 * variables.
	 * @param x x position.
	 * @param y y position.
	 * @param deltaX speed and direction for horizontal axis.
	 * @param deltaY speed and direction for vertical axis.
	 */
	public DynamicRectangleShape(int x, int y, int deltaX, int deltaY) {
		super(x,y,deltaX,deltaY);
	}

	/**
	 * Creates a ictgradschool.industry.lab12.bounce.RectangleShape instance with specified values for instance
	 * variables.
	 * @param x x position.
	 * @param y y position.
	 * @param deltaX speed (pixels per move call) and direction for horizontal
	 *        axis.
	 * @param deltaY speed (pixels per move call) and direction for vertical
	 *        axis.
	 * @param width width in pixels.
	 * @param height height in pixels.
	 */
	public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height, Color color) {
		fX = x;
		fY = y;
		fDeltaX = deltaX;
		fDeltaY = deltaY;
		fWidth = width;
		fHeight = height;
		fColor = color;
	}

	@Override
	public void paint(Painter painter) {
		painter.drawRect(fX, fY, fWidth, fHeight);
	}

}
