package ictgradschool.industry.lab12.bounce;

import java.awt.*;
import java.util.ArrayList;

/**
 * Abstract superclass to represent the general concept of a ictgradschool.industry.lab12.bounce.Shape. This class
 * defines state common to all special kinds of ictgradschool.industry.lab12.bounce.Shape instances and implements
 * a common movement algorithm. ictgradschool.industry.lab12.bounce.Shape subclasses must override method paint()
 * to handle shape-specific painting.
 * 
 * @author Ian Warren
 *
 */
public abstract class DynamicShape extends Shape{

	protected Color fColor;
	protected static final Color DEFAULT_COLOR = Color.BLACK;
	protected Boolean booleanFillRect = false;
	/**
	 * Creates a ictgradschool.industry.lab12.bounce.Shape object with default values for instance variables.
	 */
	public DynamicShape() {
		fX = DEFAULT_X_POS;
		fY = DEFAULT_Y_POS;
		fDeltaX = DEFAULT_DELTA_X;
		fDeltaY = DEFAULT_DELTA_Y;
		fWidth = DEFAULT_WIDTH;
		fHeight = DEFAULT_HEIGHT;
		fColor = DEFAULT_COLOR;
	}

	/**
	 * Creates a ictgradschool.industry.lab12.bounce.Shape object with a specified x and y position.
	 */
	public DynamicShape(int x, int y) {
		this(x, y, DEFAULT_DELTA_X, DEFAULT_DELTA_Y, DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_COLOR);
	}

	/**
	 * Creates a ictgradschool.industry.lab12.bounce.Shape instance with specified x, y, deltaX and deltaY values.
	 * The ictgradschool.industry.lab12.bounce.Shape object is created with a default width and height.
	 */
	public DynamicShape(int x, int y, int deltaX, int deltaY) {
		fX = x;
		fY = y;
		fDeltaX = deltaX;
		fDeltaY = deltaY;
		fWidth = DEFAULT_WIDTH;
		fHeight = DEFAULT_HEIGHT;
		fColor = DEFAULT_COLOR;
	}

	/**
	 * Creates a ictgradschool.industry.lab12.bounce.Shape instance with specified x, y, deltaX, deltaY, width and
	 * height values.
	 */
	public DynamicShape(int x, int y, int deltaX, int deltaY, int width, int height, Color color) {
		fX = x;
		fY = y;
		fDeltaX = deltaX;
		fDeltaY = deltaY;
		fWidth = width;
		fHeight = height;
		fColor = color;
	}


	/**
	 * Moves this ictgradschool.industry.lab12.bounce.Shape object within the specified bounds. On hitting a
	 * boundary the ictgradschool.industry.lab12.bounce.Shape instance bounces off and back into the two-
	 * dimensional world. 
	 * @param width width of two-dimensional world.
	 * @param height height of two-dimensional world.
	 */
	public Boolean dynamicMove(int width, int height) {
		int nextX = fX + fDeltaX;
		int nextY = fY + fDeltaY;

		if (nextX <= 0) {
			nextX = 0;
			fDeltaX = -fDeltaX;
			System.out.println("Touched the left wall");
			booleanFillRect = true;
		} else if (nextX + fWidth >= width) {
			nextX = width - fWidth;
			fDeltaX = -fDeltaX;
			System.out.println("Touched the right wall");
			booleanFillRect = true;
		}

		if (nextY <= 0) {
			nextY = 0;
			fDeltaY = -fDeltaY;
			System.out.println("Touched the top wall");
			booleanFillRect = false;
		} else if (nextY + fHeight >= height) {
			nextY = height - fHeight;
			fDeltaY = -fDeltaY;
			System.out.println("Touched the bottom wall");
			booleanFillRect = false;
		}

		fX = nextX;
		fY = nextY;
		//System.out.println("Boolean fillRect changed to: " + booleanFillRect);
		return booleanFillRect;
	}

	public void dynamicPaint(Painter painter, Boolean fillRect) {
		//fillRect if x <= 0 or x + fWidth >= screenWidth
		if(fillRect){
			painter.fillRect(fX,fY,fWidth,fHeight);
		}
		else {
			//drawRect otherwise
			painter.drawRect(fX, fY, fWidth, fHeight);
		}

	}

	public Color getColor(){
		return fColor;
	}

}
