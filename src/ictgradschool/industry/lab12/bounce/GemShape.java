package ictgradschool.industry.lab12.bounce;

import java.awt.*;

/**
 * Class to represent a simple rectangle.
 * 
 * @author Ian Warren
 */
public class GemShape extends Shape {

	public int[] fXPoints;
	public int[] fYPoints;
	public int fNumberPoints;
	/**
	 * Default constructor that creates a ictgradschool.industry.lab12.bounce.RectangleShape instance whose instance
	 * variables are set to default values.
	 */
	public GemShape() {
		super();
	}

	/**
	 * Creates a ictgradschool.industry.lab12.bounce.RectangleShape instance with specified values for instance
	 * variables.
	 * @param x x position.
	 * @param y y position.
	 * @param deltaX speed and direction for horizontal axis.
	 * @param deltaY speed and direction for vertical axis.
	 */
	public GemShape(int x, int y, int deltaX, int deltaY) {
		super(x,y,deltaX,deltaY);
	}

	/**
	 * Creates a ictgradschool.industry.lab12.bounce.RectangleShape instance with specified values for instance
	 * variables.
	 * @param x x position.
	 * @param y y position.
	 * @param deltaX speed (pixels per move call) and direction for horizontal
	 *        axis.
	 * @param deltaY speed (pixels per move call) and direction for vertical
	 *        axis.
	 * @param width width in pixels.
	 * @param height height in pixels.
	 */
	public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {

		fX = x;
		fY = y;
		fDeltaX = deltaX;
		fDeltaY = deltaY;
		fHeight = height;
		fWidth = width;
		fNumberPoints = 6;
		if(width <= 40){
			System.out.println("creating shape with 4 points");
			fXPoints = new int[]{fX + (fWidth/2), fX + (fWidth/2), fX + fWidth, fX + (fWidth/2), fX + (fWidth/2), fX};
			fYPoints = new int[]{fY, fY, fY + fHeight/2, fY + fHeight, fY + fHeight, fY + fHeight/2};
		}
		else{
			System.out.println("creating shape with 6 points");
			fXPoints = new int[]{fX + 20, (fX + fWidth) - 20, fWidth, (fX + fWidth)-20, fX + 20, fX};
			fYPoints = new int[]{fY, fY, fY + fHeight/2, fY + fHeight, fY + fHeight, fY + fHeight/2};
		}

	}
	
	/**
	 * Paints this ictgradschool.industry.lab12.bounce.RectangleShape object using the supplied ictgradschool.industry.lab12.bounce.Painter object.
	 */
	@Override
	public void paint(Painter painter) {
		Polygon polygon = new Polygon(fXPoints, fYPoints, fNumberPoints);
		painter.drawPolygon(polygon);
	}

	@Override
	public void move(int width, int height) {
		int nextX = fX + fDeltaX;
		int nextY = fY + fDeltaY;

		if (nextX <= 0) {
			nextX = 0;
			fDeltaX = -fDeltaX;
		} else if (nextX + fWidth >= width) {
			nextX = width - fWidth;
			fDeltaX = -fDeltaX;
		}

		if (nextY <= 0) {
			nextY = 0;
			fDeltaY = -fDeltaY;
		} else if (nextY + fHeight >= height) {
			nextY = height - fHeight;
			fDeltaY = -fDeltaY;
		}

		fX = nextX;
		fY = nextY;
		if(fWidth <= 40) {
			fXPoints = new int[]{fX + (fWidth/2), fX + (fWidth/2), fX + fWidth, fX + (fWidth/2), fX + (fWidth/2), fX};
			fYPoints = new int[]{fY, fY, fY + fHeight/2, fY + fHeight, fY + fHeight, fY + fHeight/2};
		}
		else{
			fXPoints = new int[]{fX + 20, (fX + fWidth) - 20, fX + fWidth, (fX + fWidth)-20, fX + 20, fX};
			fYPoints = new int[]{fY, fY, fY + fHeight/2, fY + fHeight, fY + fHeight, fY + fHeight/2};
		}
	}
}
