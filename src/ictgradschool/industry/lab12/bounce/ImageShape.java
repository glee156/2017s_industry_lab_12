package ictgradschool.industry.lab12.bounce;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.Shape;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

/**
 * A shape which is capable of loading and rendering images from files
 */
public class ImageShape extends ictgradschool.industry.lab12.bounce.Shape{

    private Image image;

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String fileName) throws MalformedURLException {
        super(x, y, deltaX, deltaY, width, height);
        try{
            Image image = ImageIO.read(new File(fileName));
            if (width == image.getWidth(null) && height == image.getHeight(null)) {
                this.image = image;
            } else {
                this.image = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void paint(Painter painter) {

        painter.drawImage(this.image, fX, fY, fWidth, fHeight);

    }
}
